
# namegen

Generates names by alternating vowels and consanants.

It can come up with some funny handles, but it's main purpose is 
to show and attempt to explain some basic `C`.

### compiling

The `Makefile` is very basic to keep things simple. Run `make` to compile using 
gcc, and `make clean` to delete the binary.

### running

After you run a successful `make`, the binary is in the `./bin` folder.

Run using `./bin/namegen [LOWER_RANGE] [UPPER_RANGE] [NAMES_TO_GENERATE]`

Pipe the output to a text file: `./bin/namegen 3 8 100 > 100_names.txt`

### performance

Took a free VM on [c9.io](http://c9.io) less than 1/4 of a second to generate 
1,000,000 names from 3 to 10 characters long and save them to a text file.

```
$ time ./bin/namegen 3 10 1000000 > 1MMnames.txt

real    0m0.211s
user    0m0.192s
sys     0m0.016s
```