/*
** 02 September 2015
**
** @author: cam cecil
** 
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
** (credit to the header of sqlite3).
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "namegen.h"

/*
** The main entry point of an application.
*/
int main(int argc, char *argv[]) {
    /*
    ** rand() is used in generate_name()
    ** anytime you use rand() you must seed the "random" number generator with
    ** an unsigned int through srand() which is in the stdlib library.
    */
    srand( (unsigned) time(NULL) );
    
    /*
    ** count the number of arguments given through the command line. argc
    ** includes the program name when we execute it, and we want 3 args after
    ** that. if we have less that 3+1 args, we need to educate the user on what
    ** is required of them, then return a value to exit the program.
    */
    if(argc < 4) {
        printf("usage: %s <min_len> <max_len> <names_to_gen>\n", argv[0] );
        return 0;
    }
    
    /*
    ** apply useful names to the arguments. we're also converting out arguments
    ** from char arrays to integers.
    */
    const char *app_name     =       argv[0];
    const int   min_len      = atoi( argv[1] );
    const int   max_len      = atoi( argv[2] );
    const int   names_to_gen = atoi( argv[3] );
    
    /*
    ** give some verbose output to the user about what is about to go down.
    */
    printf("%s is generating %d names between %d and %d chars:\n\n",
            app_name, names_to_gen, min_len, max_len );

    /*
    ** loop as many times as the user told us to (names_to_gen), generating
    ** a name each time using generate_name( int, int ) that we made in
    ** namegen.c, then printing it to stdout.
    */
    for(int i = 0; i < names_to_gen; i++) {
        printf("%s\n", generate_name( min_len, max_len ));
    }
    
    /*
    ** anytime you return a value in a function (unless it's a void), the function
    ** stops executing. in the case of main(), this is effectively the end of the
    ** program execution.
    */
    return 0;
}