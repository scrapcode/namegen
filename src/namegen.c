/*
** 02 September 2015
**
** @author: cam cecil
** 
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
** (credit to the header of sqlite3).
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "namegen.h"

/*
** Put our vowels and consanants into constant char arrays.
*/
const char VOWELS[]      = { 'a', 'e', 'i', 'o', 'u' };
const char CONSANANTS[]  = { 'b','c','d','f','g','h',
                             'j','k','l','m','n','p',
                             'r','s','t','v','w','x','y','z' };

/*
** Generate a name.
**
** This is a trivial implementation of a "name generator." It calculates a
** random length from a range, then generates a string that length with a random
** vowel in the even positions, and a consanant in the odd positions.
*/
char *generate_name(int low_range, int high_range) {
    /*
    ** declare and initialize the integer 'name_length'.
    ** a random number is generated and we calculate n modulus the difference between
    ** the low range and high range. this ensures the correct length after adding
    ** the low_range back.
    */
    int name_length = ( rand() % ( high_range - low_range ) ) + low_range;
    
    /*
    ** declare the char pointer 'name'.
    ** we then manually allocate the memory needed using calloc, which is a
    ** safer alternative to malloc that you will see in a lot of source.
    */
    char *name = (char *) calloc( name_length, sizeof( char ) );
    
    /*
    ** loop 'name_length' amount of times to add a new character to the name
    */
    for(int i = 0; i < name_length; i++) {
        /*
        ** if it can be divided by 2 evenly, it's in an even position in the str
        ** so we make it a vowel. if not, then let's make it a consanant.
        **
        ** we get a random value by generating a random number again, then making
        ** sure that it is less than the amount of objects in the char array.
        ** to get the amount of objects in a char array we divide the size of
        ** the arrays length in bits, by how many bits are in the array type.
        ** using the zeroeth element of the array is a dynamic way of getting
        ** the type used in the array. The formula:
        ** sizeof(arr) / sizeof(arr[0])
        */
        if(i % 2 == 0) {
            name[i] = VOWELS[( rand() % (sizeof( VOWELS ) / sizeof( VOWELS[0] )) )];
        }
        else {
            name[i] = CONSANANTS[(rand() % (sizeof( CONSANANTS ) / sizeof( CONSANANTS[0] )))];
        }
    }
    
    /*
    ** return the generated name as a pointer to the char array
    */
    return name;
}