
CC = gcc
CFLAGS = -std=c99 -Iinc

all:
	$(CC) $(CFLAGS) -o bin/namegen src/main.c src/namegen.c
	
clean:
	rm bin/namegen