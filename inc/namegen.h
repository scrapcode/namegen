/*
** 02 September 2015
**
** @author: cam cecil
** 
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
** (credit to the header of sqlite3).
*/
#ifndef NAMEGEN_H /* preprocessor definitions read by the compiler */
#define NAMEGEN_H /* this prevents including this file more than once */

#include <stdio.h>

/*
** functions and variables have to be declared before they can be used,
** so we do this in these header files through the use of what are called
** prototypes.
**
** this tells the compiler that generate_name takes two integer arguments and
** returns a pointer to a char.
*/
char *generate_name( int, int );

#endif            /* ends the ifndef directive */